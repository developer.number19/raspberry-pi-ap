#!/bin/bash

echo "Installing hostapd, dnsmasq, netfilter, iptables"
sudo apt install hostapd
sudo apt install dnsmasq
sudo DEBIAN_FRONTEND=noninteractive apt install -y netfilter-persistent iptables-persistent
#
echo "Configuring"
sudo systemctl unmask hostapd.service
sudo systemctl enable hostapd.service
#
sudo echo "interface wlan0" >> /etc/dhcpcd.conf
sudo echo "static ip_address=10.20.1.1/24" >> /etc/dhcpcd.conf
sudo echo "nohook wpa_supplicant" >> /etc/dhcpcd.conf
#
echo "Configuring IP Fwd & Nat"
sudo echo "net.ipv4.ip_forward=1" > /etc/sysctl.d/routed-ap.conf
sudo iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE
sudo netfilter-persistent save
# DHCP
sudo mv /etc/dnsmasq.conf /etc/dnsmasq.conf.old
sudo echo "interface=wlan0" > /etc/dnsmasq.conf
sudo echo "dhcp-range=10.20.1.10,10.20.1.100,255.255.255.0,300d" >> /etc/dnsmasq.conf
sudo echo "domain=wlan" >> /etc/dnsmasq.conf
sudo echo "address=/rt.wlan/10.20.1.1" >> /etc/dnsmasq.conf
# AP
sudo echo "country_code=CZ" > /etc/hostapd/hostapd.conf
sudo echo "interface=wlan0" >> /etc/hostapd/hostapd.conf
sudo echo "ssid=C0C071" >> /etc/hostapd/hostapd.conf
sudo echo "hw_mode=g" >> /etc/hostapd/hostapd.conf
sudo echo "channel=7" >> /etc/hostapd/hostapd.conf
sudo echo "macaddr_acl=0" >> /etc/hostapd/hostapd.conf
sudo echo "auth_algs=1" >> /etc/hostapd/hostapd.conf
sudo echo "ignore_broadcast_ssid=0" >> /etc/hostapd/hostapd.conf
sudo echo "wpa=2" >> /etc/hostapd/hostapd.conf
sudo echo "wpa_passphrase=c0c071c0c071" >> /etc/hostapd/hostapd.conf
sudo echo "wpa_key_mgmt=WPA-PSK" >> /etc/hostapd/hostapd.conf
sudo echo "wpa_pairwise=TKIP" >> /etc/hostapd/hostapd.conf
sudo echo "rsn_pairwise=CCMP" >> /etc/hostapd/hostapd.conf
# reboot
sudo echo "Rebooting..."
sudo reboot
